---
title: Apparence
description: ''
position: 7
category: Création de personnage
---

Si vous souhaitez avoir un aperçu des possibilités concernant votre apparence, vous pouvez vous référer à ces images.

![01](/character/character-01.webp)
![012](/character/character-02.webp)
![03](/character/character-03.webp)
![04](/character/character-04.webp)
![05](/character/character-05.webp)
