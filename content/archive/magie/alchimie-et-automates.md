---
title: Archimie et automates
description: ''
position: 3
category: Magie
---

La noble magie de Kniga est une heureuse fusion de la magie et de la technologie, qui se divise en deux domaines d’études :

- l’alchimie, qui concerne la création de potions et substances (souvent explosives) accomplissant des prodiges
- la mécanique de l’éther et la construction d’automates qui en découle.

## Jouer un alchimiste

Un alchimiste, disciple ou accompli, de Kniga dispose des compétences suivantes :

– Identifier une substance 40 %
– Créer une potion 60 %

Chaque alchimiste connaît également la recette et pos-
sède les essences du feu d’Ingramus et du passe-muraille
de Karloff qui sont des classiques (consulter le tableau
pour la description de ces deux potions). Il a juré lors
de son intronisation à l’académie de conserver le secret
le plus total sur ses connaissances.
Enfin, l’alchimiste sait parfaitement lire les runes
étranges de Kniga, quelle que soit leur difficulté. Cela
n’aide cependant en rien la lecture des lettres communes
des royaumes d’Aria, d’Aqabah et d’Osmanlie (et n’oc-
troie donc pas la compétence Lire, écrire).
Le processus alchimique
Identifier une substance est le préalable à la création
d’une potion. Un jet réussi sous cette compétence per-
met de connaître plus ou moins grossièrement l’utilité
d’une chose : « C’est une plante toxique », « C’est un
caillou qui peut brûler... », etc., donc d’orienter ensuite
la création proprement dite.
Créer une potion est l’étape suivante. À partir du
moment où l’alchimiste dispose d’une fiole de verre
vide (n’importe quel contenant peut faire l’affaire), il
essaie de créer une potion avec la substance identifiée.
La substance est détruite dans la manipulation mais
le succès de l’opération entraîne deux effets distincts :
– Tout d’abord, le meneur de jeu annonce quelle potion
l’alchimiste a créée
– Plus important, l’alchimiste possède désormais en
lui les essences pures de la potion et peut la recréer à
tout moment à condition de posséder une fiole de verre
vide, un peu de temps, et de réussir un jet sous le % de
difficulté de la potion.
Potions possibles
Vous trouverez ci-après un tableau avec quelques
exemples de potions. Nous avons délibérément laissé
certaines cases vides pour que votre alchimiste découvre
les plantes et expérimente leurs effets en leur donnant
son nom.
